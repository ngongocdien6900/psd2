const inputElement = $('.search__input');
const formElement = $('.search__form');
const resultElement = $('.search__result');
const inputValue = $('.search__value');
const searchCopyElement = $('.search__copy');


$(document).ready(() => {
    $('.menu-icon').click(() => {
        
        $('.header__menu').toggleClass('active');
    })

    formElement.on('submit', (e) => {

        e.preventDefault();
        const value = inputElement.val();
        $.ajax({
            url: 'https://api.findids.net/api/get-uid-from-username',
            method: 'POST',
            headers: {
                'authority': 'api.findids.net',
                'accept': 'application/json, text/plain, */*',
                'content-type': 'application/json;charset=UTF-8',
                'accept-language': 'vi,en;q=0.9'
            },
            dataType: "json",
            data: JSON.stringify({
                'username': value
            }),
            beforeSend: function(){

                $('.circle-loading').show();
                resultElement.hide();
                $('.copy-text').hide();
                
            },
            complete: function(){
                $('.circle-loading').hide();
            },
            success: function (data) {
                const {
                    id
                } = data.data;

                if(id.length === 0) return;
                resultElement.show();

                inputValue.val(id)
            }
        });

    })


    searchCopyElement.click(() => {

        inputValue.select();
        document.execCommand('copy');
        $('.copy-text').show();

        setTimeout(() => {
            $('.copy-text').hide();
        }, 5000)


    })
    



})
